#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    adv_attacks_clonalg.bb_targ_atks
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    This module allows to generate adversarial attacks.

    :copyright: (c) 2021 by Leonardo Lucio Custode.
    :license: MIT, see LICENSE for more details.
"""
import torch as t
import numpy as np
from tqdm import tqdm
from torchvision import datasets
from clonalselectionfactory.clonalSelectionFactory import ClonALG


device = "cuda"


class AdvAttack:
    """
    This class implements a single attack
    """

    def __init__(self, x, y, r, g, b, w, h):
        """
        Initializes the attack

        :x: The x coordinate
        :y: The y coordinate
        :r: The red channel value
        :g: The green channel value
        :b: The blue channel value
        :w: The width
        :h: The height

        """
        self._w = w
        self._h = h
        self._attacks = [[x, y, r, g, b]]
        self.affinity = 0
        self.class_ = None
        self._max_l0 = 50

    @staticmethod
    def random(width, height):
        """
        Generates a random adversarial attack

        :width: The width of the img
        :height: THe height of the img

        :returns: An AdvAttack
        """
        x = np.random.randint(0, width)
        y = np.random.randint(0, height)
        r, g, b = np.random.uniform(-1, 1, 3)

        return AdvAttack(x, y, r, g, b, width, height)

    def mutate(self, mutation_rate):
        """
        Mutate the current solution

        :mutation_rate: The mutation rate in [0, 1]
        :returns: Nothing
        """
        if len(self._attacks) < self._max_l0 and np.random.uniform() < 0.5:
            self.add_atk()
        else:
            self.mutate_atk(mutation_rate)
        self.affinity = 0

    def add_atk(self):
        """
        Adds a new perturbation on a pixel
        :returns: Nothing
        """
        equal = True
        while equal:
            equal = False
            x = np.random.randint(0, self._w)
            y = np.random.randint(0, self._h)
            r, g, b = np.random.uniform(-1, 1, 3)
                
            for ox, oy, _, _, _ in self._attacks:
                if x == ox and y == oy:
                    equal = True


        self._attacks.append([x, y, r, g, b])

    def mutate_atk(self, mutation_rate):
        """
        Mutates the current antibody

        :mutation_rate: The mutation rate \in [0, 1]
        :returns: Nothing
        """
        index = np.random.randint(0, len(self._attacks))
        to_mutate = np.random.uniform(0, 1, 5) < mutation_rate

        if to_mutate[0]:
            initial = self._attacks[index][0]
            while 0 > self._attacks[index][0] or self._attacks[index][0] > self._w:
                self._attacks[index][0] = int(np.random.normal(initial, 1))

        if to_mutate[1]:
            initial = self._attacks[index][1]
            while 0 > self._attacks[index][1] or self._attacks[index][1] > self._h:
                self._attacks[index][1] = int(np.random.normal(initial, 1))

        if to_mutate[2]:
            initial = self._attacks[index][2]
            while 0 > self._attacks[index][2] or self._attacks[index][2] > 1:
                self._attacks[index][2] = np.random.normal(initial, mutation_rate)

        if to_mutate[3]:
            initial = self._attacks[index][3]
            while 0 > self._attacks[index][3] or self._attacks[index][3] > 1:
                self._attacks[index][3] = np.random.normal(initial, mutation_rate)

        if to_mutate[4]:
            initial = self._attacks[index][4]
            while 0 > self._attacks[index][4] or self._attacks[index][4] > 1:
                self._attacks[index][4] = np.random.normal(initial, mutation_rate)


class BBAdvAtkClonALG(ClonALG):
    """
    This class implements an algorithm to generate adversarial attacks
    through clonal selection
    """

    def __init__(self, width, height):
        """
        Initializes the clonalg
        :width: the width of the img to atk
        :height: the height of the img to atk
        """
        ClonALG.__init__(self)
        self._width = width
        self._height = height
        self._max_magnitude = 255
        self._X = None
        self._y = None
        self._net = None

    def random_antibody_fcn(self):
        return AdvAttack.random(self._width, self._height)

    def calculate_affinity_fcn(self, antibodies):
        self._make_net()
        self._make_dataset()
        leaderboard = {i: (None, -1) for i in range(max(self._y) + 1)}
        for a in tqdm(antibodies):
            if a.affinity == 0:
                ratio, most_recognized, _ = self._query_net(a)
                a.affinity = ratio
                a.class_ = most_recognized

            if leaderboard[a.class_][1] < a.affinity:
                leaderboard[a.class_] = (a, a.affinity)

        with open("log.txt", "w") as f:
            for a, _ in leaderboard.values():
                if a is not None:
                    f.write(f"{a.class_};{a.affinity};{a._attacks}\n")

    def _make_net(self):
        """
        Creates the net
        :returns: A pytorch model
        """
        if self._net is None:
            self._net = t.hub.load(
                "chenyaofo/pytorch-cifar-models",
                "cifar10_resnet20",
                pretrained="cifar10"
            ).to(device)
        return self._net

    def _make_dataset(self):
        """
        Creates the dataset
        :returns: Xtrain, ytrain
        """
        if self._X is None:
            ds = datasets.CIFAR10(root='../datasets/', download=True)
            X = ds.data
            indices = np.random.choice([*range(len(X))], 2000, False)
            X = X[indices]
            self._X = t.movedim(t.Tensor(X), 3, 1).to(device)
            self._y = np.array(ds.targets)[indices]

            # Make the net see the dataset, so that batchnorm is initialized
            for i in range(10):
                out = self._net(self._X)
            yp = np.argmax(out.cpu().detach().numpy(), 1) 
            acc = np.sum(yp == self._y) / len(self._y)
            print(f"Pre-eval accuracy: {acc}")

            self._net.eval()

            out = self._net(self._X)
            acc = np.sum(np.argmax(out.cpu().detach().numpy(), 1) == self._y) / len(self._y)
            print(f"Post-eval accuracy: {acc}")

        X = t.clone(self._X)
        y = self._y
        return X, y

    def _query_net(self, antibody):
        """
        Queries the network and returns the ratio of the images
        assigned to the most assigned class

        :antibody: A AdvAttack
        :returns: A float in [0, 1]
        """
        net = self._make_net()
        X, tgt = self._make_dataset()
        X = X.to(device)
        # Add the adversarial perturbation
        a = antibody
        for x, y, r, g, b in a._attacks:
            X[:, 0, y, x] = r * self._max_magnitude
            X[:, 1, y, x] = g * self._max_magnitude
            X[:, 2, y, x] = b * self._max_magnitude
        X[X > 255] = 255
        X[X < 0] = 0
        predictions = net(X)

        argmaxes = np.argmax(predictions.cpu().detach().numpy(), axis=1)
        proportions = [np.sum(argmaxes == i) / len(X) for i in range(max(tgt) + 1)]
        most_recognized = np.argmax(proportions)

        return proportions[most_recognized], most_recognized, proportions

    def mutation_fcn(self, clones, mutation_exp):
        for i in range(0, len(clones)):
            mutation_rate = np.exp(-clones[i].affinity * mutation_exp)
            clones[i].mutate(mutation_rate)
        return clones

    def stop_criterion(self):
        return self._iteration >= 100

    def print_info(self, iteration):
        aff = [a.affinity for a in self._antibodies]
        print(f"{iteration: <5} {np.max(aff): < 5.3f} {np.mean(aff): < 5.3f} {np.std(aff): < 5.3f} {np.min(aff): < 5.3f} ")

    def remove_antibodies(self, antibodies, max_antibodies):
        best = {}
        for a in antibodies:
            if a.class_ not in best:
                best[a.class_] = []
            if len(best[a.class_]) >= max_antibodies // (1 + max(self._y)):
                # Try to distribute the antibodies uniformly in the classes
                min_ = min([b.affinity for b in best[a.class_]]) if len(best[a.class_]) > 0 else -1
                if a.affinity > min_:
                    for i in range(len(best[a.class_])):
                        if best[a.class_][i].affinity == min_:
                            best[a.class_][i] = a
                            break
            else:
                best[a.class_].append(a)

        antibodies = []
        for c, values in best.items():
            antibodies.extend(values)
        return antibodies


if __name__ == "__main__":
    ca = BBAdvAtkClonALG(32, 32)
    asd = ca.run(100, 2, 2, 0, 0, 50, verbose=True)
    print(asd)
